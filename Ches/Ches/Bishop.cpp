#include "Bishop.h"
#include "Utlis.h"


Bishop::~Bishop()
{
}


bool Bishop::CheckValidMove(string dst, vector<Piece*> board)
{
	int col = dst[0] - 'a';
	int row = dst[1] - '1';
	
	if (abs(col - (this->_col)) != abs(row - (this->_row)))
	{
		return false;
	}
	
	return !(this->CheckBlocking(dst, board));
}


bool Bishop::CheckBlocking(string dst, vector<Piece*> board)
{
	int col = dst[0] - 'a';
	int row = dst[1] - '1';
	int addCol = col > this->_col ? 1 : -1;
	int addRow = row > this->_row ? 1 : -1;
	int currRow = this->_row + addRow;
	int currCol = this->_col + addCol;
	
	while (currRow != row && currCol != col)// go for each step and check if there is something in the middle
	{
		string check = "";
		check += char(currCol + 'a');
		check += char(currRow + '1');
		if (GetPieceInPosition(check, board) != nullptr)
		{
			return true;
		}
		currCol += addCol;
		currRow += addRow;
	}
	return false;
}

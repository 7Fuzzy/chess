#pragma once
#include "Piece.h"
#include "Utlis.h"

class Bishop :
	public Piece
{
public:
	//cons't
	Bishop(int col, int row, bool player) :Piece(col, row, player) {};
	~Bishop();

	/*
	function to check if the bishop can go to that destination on the board
	input: string dst, vector<Piece*> board
	output: bool true if it valid move, else false
	*/
	virtual bool CheckValidMove(string dst, vector<Piece*> board);
	/*
	function to check if there are other pieces in the bishop way
	input: string dst, vector<Piece*> board
	output: bool true there is a piece in the way, else false
	*/
	bool CheckBlocking(string dst, vector<Piece*> board);
	
};


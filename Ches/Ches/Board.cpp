#include "Board.h"
#include "Rook.h"
#include "King.h"
#include "Knight.h"
#include "Bishop.h"
#include "Queen.h"
#include "Piece.h"
#include "Pawn.h"

#define BOARD_STR_SIZE 66
#define BOARD_LEN 8
#define LOC_LEN 2

#define VALID 0
#define VALID_CHESS 1
#define INVALID_SRC 2
#define INVALID_DST 3
#define INVALID_CHESS 4
#define INVALID_IDX 5
#define INVALID_PIC 6
#define INVALID_SAME 7

Board::Board()
{
	this->_currentPlayer = false;
	_boardStr = new char[BOARD_STR_SIZE];
	_boardStr[BOARD_STR_SIZE - 1] = NULL;
	strcpy_s(_boardStr,BOARD_STR_SIZE, "rnbkqbnrpppppppp################################PPPPPPPPRNBKQBNR1");

	for (int i = 0; i < 64; i++)
	{
		char temp = _boardStr[i];
		bool player = false;
		if (temp >= 'A' && temp <= 'Z')
		{
			temp += 'a' - 'A';
			player = true;
		}

		switch (temp)
		{
		case 'r':
			_pieces.push_back(new Rook(i % BOARD_LEN, i / BOARD_LEN, player));
			break;
		case 'n':
			_pieces.push_back(new Knight(i % BOARD_LEN, i / BOARD_LEN, player));
			break;
		case 'b':
			_pieces.push_back(new Bishop(i % BOARD_LEN, i / BOARD_LEN, player));
			break;
		case'k':
			_pieces.push_back(new King(i % BOARD_LEN, i / BOARD_LEN, player));
			break;
		case 'q':
			_pieces.push_back(new Queen(i % BOARD_LEN, i / BOARD_LEN, player));
			break;
		case 'p':
			_pieces.push_back(new Pawn(i % BOARD_LEN, i / BOARD_LEN, player));
			break;
		}
	}
}


Board::~Board()
{
}



int Board::Move(string cmd)
{
	int movmentValid = CheckMovePiece(cmd);
	if (movmentValid == VALID)
	{
		Piece* pieceToMove = GetPieceInPosition(cmd.substr(0, LOC_LEN), _pieces);

		int srcRow = pieceToMove->GetRow(), srcCol = pieceToMove->GetColumn();
		int dstRow = (cmd[3] - '1'), dstCol = (cmd[2] - 'a');
		
		pieceToMove->SetColumn(dstCol);
		pieceToMove->SetRow(dstRow);
		

		Kill(cmd.substr(2, LOC_LEN), pieceToMove);

		char oldDst = _boardStr[dstCol + dstRow * BOARD_LEN];
		_boardStr[dstCol + dstRow * BOARD_LEN] = _boardStr[srcCol + srcRow * BOARD_LEN];
		_boardStr[srcCol + srcRow * BOARD_LEN] = '#';

		if (CheckChessOnPlayer(_currentPlayer))
		{
			_boardStr[srcCol + srcRow * BOARD_LEN] = _boardStr[dstCol + dstRow * BOARD_LEN];
			_boardStr[dstCol + dstRow * BOARD_LEN] = oldDst;

			pieceToMove->SetColumn(srcCol);
			pieceToMove->SetRow(srcRow);

			return INVALID_CHESS;
		}

		_currentPlayer = !_currentPlayer;
		return CheckChessOnPlayer(!_currentPlayer) ? VALID_CHESS : VALID;
	}
	return movmentValid;
}

void Board::Kill(string dst, Piece* killer)
{
	int row = dst[1] - '1';
	int col = dst[0] - 'a';
	bool notKilled = true;

	int i = 0;
	for (i = 0; i < _pieces.size() && notKilled; i++)
	{
		if (_pieces[i] != killer && _pieces[i]->GetRow() == row && _pieces[i]->GetColumn() == col)
		{
			delete _pieces[i];
			_pieces.erase(_pieces.begin() + i);
			notKilled = false;
		}
	}
}

bool Board::CheckChessOnPlayer(bool player)
{
	Piece* King = GetKingOfPlayer(player);
	string kingPos = "00";
	kingPos[1] = King->GetRow() + '1';
	kingPos[0] = King->GetColumn() + 'a';

	for (int i = 0; i < _pieces.size(); i++)
	{
		if (_pieces[i]->GetPlayer() != player && _pieces[i]->CheckValidMove(kingPos, _pieces))
		{
			return true;
		}
	}

	return false;
}

int Board::CheckMovePiece(string cmd)
{
	if (cmd.substr(0, LOC_LEN) == cmd.substr(2, LOC_LEN))
	{
		return INVALID_SAME;
	}

	//Check If source is valid
	int srcValid = CheckSourceValid(cmd.substr(0, LOC_LEN));
	
	//Check if destination is valid
	int dstValid = CheckDestValid(cmd.substr(2, LOC_LEN));

	//Getting the wanted piece
	Piece* srcPiece = GetPieceInPosition(cmd.substr(VALID, LOC_LEN), _pieces);

	//It only depends on the piece rules
	if (srcValid == VALID && dstValid == VALID)
	{
		return srcPiece->CheckValidMove(cmd.substr(2, LOC_LEN), _pieces) ? VALID : INVALID_PIC;
	}
	else if (dstValid == VALID) //Source invalid
	{
		return srcValid;
	}
	else //dst invalid
	{
		return dstValid;
	}
}


/*
	Function checks if there is a piece of the current player in the given position
	and returns Valid (0) or Invalid (2)
	input: Position (string)
	output: valid or not
*/
int Board::CheckSourceValid(string src)
{
	Piece* srcPiece = GetPieceInPosition(src, this->_pieces);
	if (srcPiece != nullptr && srcPiece->GetPlayer() == this->_currentPlayer)
	{
		return VALID;
	}
	return INVALID_SRC;
}


/*
	Function checks if there isnt a piece of the current player in the given position
	and returns Valid (0) or Invalid (3)
	input: Position (string)
	output: valid or not
*/
int Board::CheckDestValid(string dst)
{
	Piece* p = GetPieceInPosition(dst, _pieces);
	if (p == nullptr || p->GetPlayer() != _currentPlayer)
	{
		return VALID;
	}
	return INVALID_DST;
	
} 


/*
	Function returns the king of the given player. (or nullptr in case it wasnt found. if that happens theres a bug)
	input: player
	output: king
*/
Piece* Board::GetKingOfPlayer(bool player)
{
	char wantedKing = player ? 'K' : 'k';
	for (int i = 0; i < strlen(_boardStr); i++)
	{
		if (_boardStr[i] == wantedKing)
		{
			string pos = "00";
			pos[0] = i % BOARD_LEN + 'a';
			pos[1] = i / BOARD_LEN + '1';
			return GetPieceInPosition(pos, _pieces);
		}
	}

	return nullptr;
}


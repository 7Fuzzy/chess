#pragma once
#include "Piece.h"
#include "Utlis.h"
#include "King.h"

#include <iostream>
#include <vector>
using namespace std;

class Board
{
public:
	Board();
	~Board();

	int Move(string cmd);

	bool CheckChessOnPlayer(bool player);
	int CheckMovePiece(string cmd);
	int CheckSourceValid(string src);
	int CheckDestValid(string dst);
	


private:
	vector<Piece*> _pieces;
	bool _currentPlayer;
	char* _boardStr;

	void Kill(string dst, Piece* killer);
	Piece* GetKingOfPlayer(bool player);
};


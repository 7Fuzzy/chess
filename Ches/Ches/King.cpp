#include "King.h"

King::~King()
{
}

bool King::CheckValidMove(string dst, vector<Piece*> board)
{
	int row = dst[1] - '1';
	int col = dst[0] - 'a';

	return (abs(row - this->_row) <= 1 && abs(col - this->_col <= 1));
}

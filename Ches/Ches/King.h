#pragma once
#include "Piece.h"
#include "Utlis.h"
#include <math.h>

class King :
	public Piece
{
public:
	King(int col, int row, bool player) :Piece(col, row, player) {};
	~King();

	/*
	function to check if the king can go to that destination on the board
	input: string dst, vector<Piece*> board
	output: bool true if it valid move, else false
	*/
	virtual bool CheckValidMove(string dst, vector<Piece*> board);

};


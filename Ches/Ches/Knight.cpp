#include "Knight.h"



bool Knight::CheckValidMove(string dst, vector<Piece*> board)
{
	int row = dst[1] - '1';
	int col = dst[0] - 'a';

	bool rule1 = abs(row - _row) == 2 && abs(col - _col) == 1;
	bool rule2 = abs(row - _row) == 1 && abs(col - _col) == 2;

	return rule1 || rule2;
}


Knight::~Knight()
{
}

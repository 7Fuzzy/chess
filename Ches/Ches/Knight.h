#pragma once
#include "Piece.h"
#include "Utlis.h"
#include <math.h>

class Knight :
	public Piece
{
public:
	Knight(int col, int row, bool player) :Piece(col, row, player) {};
	~Knight();
	/*
	function to check if the knight can go to that destination on the board
	input: string dst, vector<Piece*> board
	output: bool true if it valid move, else false
	*/
	virtual bool CheckValidMove(string dst, vector<Piece*> board);
};


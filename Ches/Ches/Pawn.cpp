#include "Pawn.h"



bool Pawn::CheckValidMove(string dst, vector<Piece*> board)
{
	int startDeltaRow = _player ? -1 : 1;
	int deltaRow = startDeltaRow;
	int row = dst[1] - '1';
	int col = dst[0] - 'a';

	if ((_player && _row == 6) || (!_player && _row == 1))//check if it in the start line
	{
		startDeltaRow = _player ? -2 : 2;//it can mov
	}
	//chechk for each rule
	bool rule = (_row + startDeltaRow == row || _row + deltaRow == row) ; 

	//make sure the destination is empty or there is an enemy there
	if (GetPieceInPosition(dst, board) == nullptr)
	{
		return rule && _col == col;
	}
	else if (GetPieceInPosition(dst, board)->GetPlayer() != _player)//it can kill just if there is an enemy
	{
		return rule && abs(col - _col) == 1;;
	}
	return false;
	
}


Pawn::~Pawn()
{
}

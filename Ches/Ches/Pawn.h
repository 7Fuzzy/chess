#pragma once
#include "Piece.h"
#include "Utlis.h"
#include <math.h>

class Pawn :
	public Piece
{
public:
	Pawn(int col, int row, bool player) :Piece(col, row, player) {};
	~Pawn();

	/*
	function to check if the pawn can go to that destination on the board
	input: string dst, vector<Piece*> board
	output: bool true if it valid move, else false
	*/
	virtual bool CheckValidMove(string dst, vector<Piece*> board);
};


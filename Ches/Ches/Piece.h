#pragma once
#include <string>
#include <vector>
using namespace std;


class Piece
{
protected:
	int _col;
	int _row;
	bool _player;

public:
	Piece(int col, int row, bool player) : _col(col), _row(row), _player(player) {};
	~Piece();

	virtual bool CheckValidMove(string dst, vector<Piece*> board) = 0;
	int GetColumn() { return this->_col; };
	int GetRow() { return this->_row; };
	bool GetPlayer() { return this->_player; };
	void SetRow(int row) { this->_row = row; };
	void SetColumn(int column) { this->_col = column; };
	

};


#include "Queen.h"

bool Queen::CheckValidMove(string dst, vector<Piece*> board)
{
	int row = dst[1] - '1';
	int col = dst[0] - 'a';

	if (row == this->_row || col == this->_col)
	{
		return !this->StraightBlocking(dst, board);
	}
	else if (abs(this->_row - row) == abs(this->_col - col))
	{
		return !this->DiagnalBlocking(dst, board);
	}
	else
	{
		return false;
	}
}


bool Queen::StraightBlocking(string dst, vector<Piece*> board)
{
	int col = dst[0] - 'a';
	int row = dst[1] - '1';

	if (col == this->_col)//go vertical
	{
		int add = row > this->_row ? 1 : -1;
		int curr = this->_row + add;

		while (curr != row)//go for each step and check if there is piece there
		{
			string check = "";
			check += char(this->_col + 'a');
			check += char(curr + '1');
			if (GetPieceInPosition(check, board) != nullptr)
			{
				return true;
			}
			curr += add;
		}
		return false;
	}
	else //go horizontal
	{
		int add = col > this->_col ? 1 : -1;
		int curr = this->_col + add;

		while (curr != col)//go for each step and check if there is piece there
		{
			string check = "";
			check += char(curr + 'a');
			check += char(this->_row + '1');
			if (GetPieceInPosition(check, board) != nullptr)
			{
				return true;
			}
			curr += add;
		}
		return false;
	}
}

bool Queen::DiagnalBlocking(string dst, vector<Piece*> board)
{
	int col = dst[0] - 'a';
	int row = dst[1] - '1';
	int addCol = col > this->_col ? 1 : -1;
	int addRow = row > this->_row ? 1 : -1;
	int currRow = this->_row + addRow;
	int currCol = this->_col + addCol;

	while (currRow != row && currCol != col)//go for each step and check if there is piece there
	{
		string check = "";
		check += char(currCol + 'a');
		check += char(currRow + '1');
		if (GetPieceInPosition(check, board) != nullptr)
		{
			return true;
		}
		currCol += addCol;
		currRow += addRow;
	}
	return false;
}


Queen::~Queen()
{
}

#pragma once
#include "Piece.h"
#include "Utlis.h"
#include <math.h>


class Queen :public Piece
{
public:
	Queen(int col, int row, bool player) :Piece(col, row, player) {};
	~Queen();

	/*
	function to check if the queen can go to that destination on the board
	input: string dst, vector<Piece*> board
	output: bool true if it valid move, else false
	*/
	virtual bool CheckValidMove(string dst, vector<Piece*> board);

	/*
	function to check if there are other pieces in the queen way(Straight way)
	input: string dst, vector<Piece*> board
	output: bool true there is a piece in the way, else false
	*/
	bool StraightBlocking(string dst, vector<Piece*> board);
	/*
	function to check if there are other pieces in the queen way(Diagnal way)
	input: string dst, vector<Piece*> board
	output: bool true there is a piece in the way, else false
	*/
	bool DiagnalBlocking(string dst, vector<Piece*> board);
};


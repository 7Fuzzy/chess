#include "Rook.h"
#include "Utlis.h"


Rook::~Rook()
{
}


/*
	Function checks if the desired movment is valid and returns the moving status
	input: destination cube
	output: status
*/
bool Rook::CheckValidMove(string dst, vector<Piece*> board)
{
	int col = dst[0] - 'a';
	int row = dst[1] - '1';
	//check if the rook walk harizontal or vertical
	if (col == this->_col || this->_row == row)
	{
		return !this->CheckBlocking(dst, board);
	}
	else
	{
		return false;
	}
}

bool Rook::CheckBlocking(string dst, vector<Piece*> board)
{
	int col = dst[0] - 'a';
	int row = dst[1] - '1';

	if (col == this->_col)//check if to move vertical
	{
		int add = row > this->_row ? 1 : -1;
		int curr = this->_row + add;

		while (curr != row)//go for each step of the rook
		{
			string check = "";
			check += char(this->_col + 'a');
			check += char(curr + '1');
			if (GetPieceInPosition(check, board) != nullptr)//if there is a piece in the 
			{
				return true;
			}
			curr += add;
		}
		return false;
	}
	else if (row == this->_row)//check if to move horizontly
	{
		int add = col > this->_col ? 1 : -1;
		int curr = this->_col + add;

		while (curr != col)//go for each step of the rook
		{
			string check = "";
			check += char(curr + 'a');
			check += char(this->_row + '1');
			if (GetPieceInPosition(check, board) != nullptr)
			{
				return true;
			}
			curr += add;
		}
		return false;
	}

	return false;
}

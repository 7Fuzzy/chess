#pragma once
#include "Piece.h"
class Rook :
	public Piece
{
public:
	bool CheckBlocking(string dst, vector<Piece*> board);
	/*
	function to check if the rook can go to that destination on the board
	input: string dst, vector<Piece*> board
	output: bool true if it valid move, else false
	*/
	virtual bool CheckValidMove(string dst, vector<Piece*> board);
	Rook(int col, int row, bool player) :Piece(col, row, player) {};
	~Rook();
};


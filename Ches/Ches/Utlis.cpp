#include "Utlis.h"


Piece* GetPieceInPosition(string pos, vector<Piece*> pieces)
{
	int row = pos[1] - '1';
	int col = pos[0] - 'a';

	int i = 0;
	for (i = 0; i < pieces.size(); i++)//check for each piece in the board
	{	//check if the current piece is in the position
		if (pieces[i]->GetRow() == row && pieces[i]->GetColumn() == col)
		{
			return pieces[i];
		}
	}

	return nullptr; //return null if there is no piece in the position
}


